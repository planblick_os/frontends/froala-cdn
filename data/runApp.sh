#!/bin/sh
_term() {
  echo "Caught SIGTERM signal!"
}

_termint() {
  echo "Caught SIGINT signal!"
}

trap _term SIGTERM
trap _termint SIGINT

curl --location --request POST 'http://kong.planblick.svc:8001/services/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "name": "froala-cdn",
  "url": "http://froala.planblick.svc:80"
}'


curl --location --request POST 'http://kong.planblick.svc:8001/services/froala-cdn/routes/' \
--header 'Content-Type: application/json' \
--data-raw '{
  "hosts": ["froala.serviceblick.com", "froala.testblick.de"]
}'

nginx