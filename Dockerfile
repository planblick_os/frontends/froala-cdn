FROM yobasystems/alpine-nginx
MAINTAINER Florian Kroeber @ https://www.planBLICK.com

RUN apk update && apk add --no-cache nodejs npm curl

WORKDIR /usr/src/app
RUN npm install froala-editor

RUN cp -r /usr/src/app/node_modules/froala-editor/html /etc/nginx/html/
RUN cp -r /usr/src/app/node_modules/froala-editor/css /etc/nginx/html/
RUN cp -r /usr/src/app/node_modules/froala-editor/img /etc/nginx/html/
RUN cp -r /usr/src/app/node_modules/froala-editor/js /etc/nginx/html/
RUN cp /usr/src/app/node_modules/froala-editor/index.html /etc/nginx/html/index.html

COPY ./data/nginx/nginx.conf /etc/nginx/nginx.conf

COPY ./data/runApp.sh ./runApp.sh
RUN chmod 777 ./runApp.sh && chmod +x ./runApp.sh
CMD ["./runApp.sh"]
